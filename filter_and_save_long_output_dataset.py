import instructionBertClass as instructionBertClass
import dataset_methods as dataset_methods
import csv
from sys import argv

script, model_name, max_length = argv

def filter_redundant(dataset_text):
  # check for duplicates in instruction or answers
  unique_dataset = []
  instructions = set()
  answers = set()
  for row in dataset_text:
    if not row[0] in instructions and not row[1] in answers:
      instructions.add(row[0])
      answers.add(row[1])
      unique_dataset.append(row)

  return unique_dataset


def save_filtered_dataset(model_name):
    monolingualInstructionBERT = instructionBertClass.instructionBERT(model_name, max_length = max_length)

    plain_filtered_dataset_text = []
    dataset_methods.get_orca_dataset(monolingualInstructionBERT, plain_filtered_dataset_text, save_tokenized=False)
    print(plain_filtered_dataset_text[-1])
    dataset_methods.get_lima_dataset(monolingualInstructionBERT, plain_filtered_dataset_text, save_tokenized=False)
    print(plain_filtered_dataset_text[-1])
    dataset_methods.get_gpt4all_dataset(monolingualInstructionBERT, plain_filtered_dataset_text, save_tokenized=False)
    print(plain_filtered_dataset_text[-1])
    dataset_methods.get_ultrachat_dataset(monolingualInstructionBERT, plain_filtered_dataset_text, save_tokenized=False)
    print(plain_filtered_dataset_text[-1])
    dataset_methods.get_roleBench_dataset(monolingualInstructionBERT, plain_filtered_dataset_text, save_tokenized=False)
    print(plain_filtered_dataset_text[-1])
    dataset_methods.get_evol_instruct_dataset(monolingualInstructionBERT, plain_filtered_dataset_text, save_tokenized=False)
    print(plain_filtered_dataset_text[-1])
    dataset_methods.get_alpaca_gpt4_dataset(monolingualInstructionBERT, plain_filtered_dataset_text, save_tokenized=False)
    print(plain_filtered_dataset_text[-1])
    dataset_methods.get_agent_instruct_dataset(monolingualInstructionBERT, plain_filtered_dataset_text, save_tokenized=False)
    print(plain_filtered_dataset_text[-1])

    print("size of the accumulated dataset before discarding dublicates:")
    print(len(plain_filtered_dataset_text))
    unique_dataset = filter_redundant(plain_filtered_dataset_text) #  3637 - 1556 = 2081 filtered from agent instruct

    # save plain text as csv
    print("size of the final accumulated dataset:")
    print(len(unique_dataset))
    model_name_without_path = model_name.split("/")[-1]
    file_name = model_name_without_path + "_long_output_dataset.csv"
    with open(file_name,'w') as out:
        csv_out=csv.writer(out)
        #csv_out.writerow(['Instruction','Output'])
        for row in unique_dataset:
            csv_out.writerow(row)

    return plain_filtered_dataset_text

data_list = save_filtered_dataset(model_name)
