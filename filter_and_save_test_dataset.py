import instructionBertClass as instructionBertClass
import dataset_methods as dataset_methods
import datasets
import csv
from sys import argv

script, model_name, flan_part = argv

def save_filtered_dataset(model_name, flan_part):
    monolingualInstructionBERT = instructionBertClass.instructionBERT(model_name)

    plain_filtered_dataset_text = []
    dataset_methods.get_orca_dataset(monolingualInstructionBERT, plain_filtered_dataset_text, save_tokenized=False)
    dataset_methods.get_platypus_dataset(monolingualInstructionBERT, plain_filtered_dataset_text, save_tokenized=False)
    dataset_methods.get_lima_dataset(monolingualInstructionBERT, plain_filtered_dataset_text, save_tokenized=False)
    dataset_methods.get_helper_dataset(monolingualInstructionBERT, plain_filtered_dataset_text, save_tokenized=False)
    dataset_methods.get_flan_dataset(monolingualInstructionBERT, plain_filtered_dataset_text, flan_part, save_tokenized=False)

    # save plain text as csv
    print(len(plain_filtered_dataset_text))
    file_name = model_name + "_" + flan_part + "_dataset.csv"
    with open(file_name,'w') as out:
        csv_out=csv.writer(out)
        #csv_out.writerow(['Instruction','Output'])
        for row in plain_filtered_dataset_text:
            csv_out.writerow(row)

    return plain_filtered_dataset_text

data_list = save_filtered_dataset(model_name, flan_part)
