import instructionBertClass
from dataset_methods import get_platypus_dataset, get_lima_dataset, get_helper_dataset, get_flan_dataset
import datasets
import csv

def run_preparation(model_name, flan_part):
    monolingualInstructionBERT = instructionBertClass.instructionBERT(model_name)

    plain_filtered_dataset_text = []
    list_platypus = get_platypus_dataset(monolingualInstructionBERT, plain_filtered_dataset_text)
    list_lima = get_lima_dataset(monolingualInstructionBERT, plain_filtered_dataset_text)
    list_helper = get_helper_dataset(monolingualInstructionBERT, plain_filtered_dataset_text)
    list_flan = get_flan_dataset(monolingualInstructionBERT, plain_filtered_dataset_text, flan_part)

    # save plain text as csv
    print(len(plain_filtered_dataset_text))
    with open('filtered_instruction_dataset.csv','w') as out:
        csv_out=csv.writer(out)
        csv_out.writerow(['Instruction','Output'])
        for row in plain_filtered_dataset_text:
            csv_out.writerow(row)


    # create the datasets from the lists
    full_train_list = list_lima + list_helper + list_flan + list_platypus
    full_dataset = datasets.Dataset.from_list(full_train_list)
    # shuffle with a seed and split into train and validation
    shuffled_dataset = full_dataset.shuffle(seed=42)
    validation_dataset = full_dataset.select(range(100))
    print(validation_dataset)
    train_dataset = full_dataset.select(range(100, len(full_dataset)))
    print(train_dataset)

    return monolingualInstructionBERT, validation_dataset, train_dataset
