# InstructionBERT to [explain attention in instruction-tuned models](https://gitlab.com/Bachstelze/instructionbert/-/blob/main/Explainable_attention_in_instruction_tuned_models.pdf)

Let's try to train a minimalistic instruction model with an already good analysed and pretrained encoder like BERT.

So we can research the [Bertology](https://aclanthology.org/2020.tacl-1.54.pdf) with instruction-tuned models and investigate [what happens to BERT embeddings during fine-tuning](https://aclanthology.org/2020.blackboxnlp-1.4.pdf) and which [knowledge is forgotten](https://aclanthology.org/2020.blackboxnlp-1.17.pdf). The example notebooks with BertViz are in the analysis folder.

Huggingface has an API for [warm-starting](https://huggingface.co/blog/warm-starting-encoder-decoder) [BertGeneration](https://huggingface.co/docs/transformers/model_doc/bert-generation) with [Encoder-Decoder-Models](https://huggingface.co/docs/transformers/v4.35.2/en/model_doc/encoder-decoder) for this purpose.

## Training a robust multilingual model

* export CUDA_VISIBLE_DEVICES=0
* source bin/activate
* pip install -r ./requirements.txt
* edit the supported languages in load_aya_dataset.py
* change the model name to "FacebookAI/xlm-roberta-base" to load it from huggingface
* python3 load_aya_dataset.py "/media/data/models/xlm-roberta-base" 1024
* python3 test_train_multilingual.py "/media/data/models/xlm-roberta-base" 1024 True "adamw_bnb_8bit" 1 8 20000 0.0001 2 8
* those ordered training parameters are changeable: model_name, maximal_length, bool_fp16, optimizer, dataloader_workers, batch_size, warmup, lr, accumulation, epochs