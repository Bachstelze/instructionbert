import torch
from transformers import AutoTokenizer, AutoModel, EncoderDecoderConfig, EncoderDecoderModel

class instructionBERT:
  """
  instructionBERT is a shared BERT as encoder-decoder-model,
  so that the encoder and decoder are tied together for a faster convergence with a smaller model size.
  """
  def __init__(self, path_name, ngram_size = 5, length_penalty = 1, num_beams = 16, max_length = 512, output_attention=False):
    """
    The model gets intialized and configurated

    :param str path_name: The path to a local model or the name in the online hub
    :param int ngram_size: The ngram size how often a generate token can be repeated
    :param float length_penalty:
      Exponential penalty to the length that is used with beam-based generation.
      1.0 means no penalty.
      Set to values < 1.0 in order to encourage the model to generate shorter sequences,
      to a value > 1.0 in order to encourage the model to produce longer sequences.
    """
    self.max_length = int(max_length)
    self.path_name = path_name
    # preload the pretrained model to get the max_length
    pretrained_model = AutoModel.from_pretrained(self.path_name)
    self.tokenizer =  AutoTokenizer.from_pretrained(path_name, max_length=int(max_length), model_max_length=self.max_length)

    if max_length == pretrained_model.config.max_position_embeddings: # the max length is already exactly defined
      # directly initialize the shared encoder decoder model
      self.sharedModel = EncoderDecoderModel.from_encoder_decoder_pretrained(path_name, path_name, tie_encoder_decoder=True, output_attentions=output_attention)
    else: # the pretrained max length differs
      # set the new max length
      pretrained_model.config.max_position_embeddings = self.max_length
      # use the updated model as config
      config = EncoderDecoderConfig.from_encoder_decoder_configs(pretrained_model.config, pretrained_model.config, tie_encoder_decoder=True, output_attentions=output_attention)
      self.sharedModel = EncoderDecoderModel(config=config)
      # set the max length again
      # it is unclear if this is necessary
      self.sharedModel.config.encoder.max_length = self.max_length
      self.sharedModel.config.decoder.max_length = self.max_length
      self.sharedModel.config.max_length=self.max_length

    # configuration of the model to use the special tokens of the pretraining
    self.sharedModel.config.decoder_start_token_id = self.tokenizer.cls_token_id
    self.sharedModel.config.eos_token_id = self.tokenizer.sep_token_id
    self.sharedModel.config.pad_token_id = self.tokenizer.pad_token_id
    self.sharedModel.config.unk_token_id = self.tokenizer.unk_token_id
    self.sharedModel.config.vocab_size = self.sharedModel.config.encoder.vocab_size

    # configuration of the generation
    # TODO create a callable function with those parameters
    # https://medium.com/analytics-vidhya/fine-tune-a-roberta-encoder-decoder-model-trained-on-mlm-for-text-generation-23da5f3c1858
    """
    self.sharedModel.config.no_repeat_ngram_size = ngram_size
    self.sharedModel.config.early_stopping = False
    self.sharedModel.config.length_penalty = length_penalty
    self.sharedModel.config.num_beams = num_beams
    """

  def tokenize_function(self, untokenized):
    """
    tokenize the untokenized string with the defined special tokens and truncate to the max length
    """
    return self.tokenizer(untokenized, padding="max_length", truncation=True, add_special_tokens=True, max_length = self.max_length)


  def info(self):
    """
    print some useful informations
    """
    print("all special tokens:")
    print(self.tokenizer.all_special_tokens)
    print("\nmodel parameters:")
    print(self.sharedModel.num_parameters())
    print("\nthe tokenizer is fast")
    print(self.tokenizer.is_fast)
    print("\nmodel configuration of " + self.path_name)
    print(self.sharedModel.config)
    print(self.sharedModel)
