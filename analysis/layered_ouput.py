from bertviz import model_view, head_view
from transformers import AutoModel, AutoTokenizer
import os
import csv
import sys

def get_file_size(data):
    # Retrieve the size of the file in bytes
    file_size = sys.getsizeof(data)

    # Convert file size to kilobytes
    kb = file_size / 1024
    # Convert kilobytes to megabytes
    mb = kb / 1024

    print(mb)
    return mb

# reading data from a csv file 'Data.csv'
with open('1_bertViz_01temp.csv', newline='') as file:

    reader = csv.reader(file, delimiter = ',')

    # store the headers in a separate variable,
    # move the reader object to point on the next row
    headings = next(reader)
    # output list to store all rows
    instruction_tuples = []
    for row in reader:
        instruction_tuples.append(row[:])

#create example folders

for i in range(len(instruction_tuples)):
    os.mkdir(str(i)+"_example")
    for layer in range(12):
        os.mkdir(str(i) +"_example/" + str(layer)+"_layer")
        for head in range(12):
            os.mkdir(str(i) +"_example/" + str(layer)+"_layer/" + str(head)+"_head")

bert_models = [
"bert-base-uncased",
"royokong/sup-PromptBERT",
"royokong/unsup-PromptBERT",
"../yupiter notebooks/instructionBERTencoder",
"bert-base-multilingual-cased"]

roberta_models = ["roberta-base",
"royokong/sup-PromptRoBERTa",
"royokong/unsup-PromptRoBERTa",
"../yupiter notebooks/instructionROBERTAencoder",
# "google/bigbird-roberta-base",
"xlm-roberta-base"]

def save_views(attention, tokens, folder):
    folder = folder + "_example"
    html_model_view = model_view(attention, tokens, html_action='return')
    file_name = folder + "/" + model_name.replace("/","_") + "_model_view.html"
    with open(file_name, 'w') as file:
        size = get_file_size(html_model_view.data)
        if size < 100:
            file.write(html_model_view.data)
        else:
            print(str(folder))

    for layer_number in range(12):
        html_model_view = model_view(attention, tokens, html_action='return' ,include_layers=[layer_number])
        file_name = folder + "/" + str(layer_number) + "_layer/" + model_name.replace("/","_") +  "_layer_view.html"

        # save complete layer
        with open(file_name, 'w') as file:
            size = get_file_size(html_model_view.data)
            if size < 100:
                file.write(html_model_view.data)
            else:
                print(str(folder) + ":" +str(layer_number))

        # save single heads
        for head_number in range(12):
            html_model_view = model_view(attention, tokens, html_action='return' ,include_layers=[layer_number], include_heads=[head_number])
            file_name = folder + "/" + str(layer_number) + "_layer/" + str(head_number) + "_head/" +  model_name.replace("/","_") +  "_single_head_view.html"

            # save complete layer
            with open(file_name, 'w') as file:
                size = get_file_size(html_model_view.data)
                file.write(html_model_view.data)


    """
    html_head_view = head_view(attention, tokens, html_action='return')
    file_name = folder + "/" + model_name.replace("/","_") + "_head_view.html"
    with open(file_name, 'w') as file:
        size = get_file_size(html_model_view.data)
        if size < 100:
            file.write(html_head_view.data)

    for layer_number in range(12):
        html_head_view = head_view(attention, tokens, html_action='return' ,include_layers=[layer_number])
        file_name = folder + "/" + str(layer_number) + "_layer/" + model_name.replace("/","_") +  "_head_view.html"
        with open(file_name, 'w') as file:
            size = get_file_size(html_model_view.data)
            if size < 100:
                file.write(html_head_view.data)
    """

def generate_bert_viz(model, sentence_a, sentence_b, folder):
    model = AutoModel.from_pretrained(model_name, output_attentions=True)
    tokenizer = AutoTokenizer.from_pretrained(model_name)
    inputs = tokenizer.encode_plus(sentence_a, sentence_b, return_tensors='pt')
    input_ids = inputs['input_ids']
    token_type_ids = inputs['token_type_ids']
    attention = model(input_ids, token_type_ids=token_type_ids)[-1]
    sentence_b_start = token_type_ids[0].tolist().index(1)
    input_id_list = input_ids[0].tolist() # Batch index 0
    tokens = tokenizer.convert_ids_to_tokens(input_id_list)

    save_views(attention, tokens, folder)

for model_name in bert_models:
    for index, instruction_tuple in enumerate(instruction_tuples):
        generate_bert_viz(model_name, instruction_tuple[0], instruction_tuple[1], str(index))


def generate_roberta_viz(model, sentence_a, sentence_b, folder):
    model = AutoModel.from_pretrained(model_name, output_attentions=True)  # Configure model to return attention values
    tokenizer = AutoTokenizer.from_pretrained(model_name)
    inputs = tokenizer.encode(sentence_a, sentence_b, return_tensors='pt')  # Tokenize input text
    sentence_b_start = inputs[0].tolist().index(2) + 1 # the sentence b somehow starts with </s>
    outputs = model(inputs)  # Run model
    attention = outputs[-1]  # Retrieve attention from model outputs
    tokens = tokenizer.convert_ids_to_tokens(inputs[0])  # Convert input ids to token strings

    save_views(attention, tokens, folder)

for model_name in roberta_models:
    for index, instruction_tuple in enumerate(instruction_tuples):
        generate_roberta_viz(model_name, instruction_tuple[0], instruction_tuple[1], str(index))
