import datasets
import requests
import json

def tokenize_function(instructionModel, untokenized):
  # only used by the scripts to tokenize unstreamed smaller datasets
  return instructionModel.tokenizer(untokenized, padding="max_length", truncation=True, add_special_tokens=True)

def stream_tokenized_dataset(dataset, tokenize, pad_id):
  """
  Tokenize one filtered dataset and yield the tokenized dataset.
  """

  for number, line in enumerate(dataset):
    input = line[0]
    target = line[1]

    tokenized_inputs = tokenize(input)
    tokenized_outputs = tokenize(target)
    # copy the tokenized to the tokenized inputs
    tokenized_inputs["decoder_attention_mask"] = tokenized_outputs.attention_mask
    tokenized_inputs["labels"] = tokenized_outputs.input_ids.copy()

    # because BERT automatically shifts the labels, the labels correspond exactly to `decoder_input_ids`.
    # For the purpose of training instructionBERT, we compute the loss only on the generated word tokens
    # and make sure that the PAD token is ignored by the loss function
    tokenized_inputs["labels"] = [-100 if token == pad_id else token for token in tokenized_inputs["labels"]]

    # append the combined tokenized input and output to the list
    yield tokenized_inputs
    
def get_tokenized_datasets(instructionModel, dataset):
  """
  Tokenize one filtered dataset and return the tokenized train and validation dataset.
  """
  tokenized_dataset_list = []
  for number, line in enumerate(dataset):
    input = line[0]
    target = line[1]

    tokenized_inputs = tokenize_function(instructionModel, input)
    tokenized_outputs = tokenize_function(instructionModel, target)
    # copy the tokenized to the tokenized inputs
    tokenized_inputs["decoder_attention_mask"] = tokenized_outputs.attention_mask
    tokenized_inputs["labels"] = tokenized_outputs.input_ids.copy()

    # because BERT automatically shifts the labels, the labels correspond exactly to `decoder_input_ids`.
    # For the purpose of training instructionBERT, we compute the loss only on the generated word tokens
    # and make sure that the PAD token is ignored by the loss function
    pad_id = instructionModel.sharedModel.config.pad_token_id
    tokenized_inputs["labels"] = [-100 if token == pad_id else token for token in tokenized_inputs["labels"]]
    
    # append the combined tokenized input and output to the list
    tokenized_dataset_list.append(tokenized_inputs)

  full_dataset = datasets.Dataset.from_list(tokenized_dataset_list)
  # shuffle with a seed and split into train and validation
  shuffled_dataset = full_dataset.shuffle(seed=42)
  validation_dataset = full_dataset.select(range(100))
  print(validation_dataset)
  train_dataset = full_dataset.select(range(100, len(full_dataset)))
  print(train_dataset)

  return validation_dataset, train_dataset

def get_tokenized_dict(instructionModel, input, target, encode_unk=True):
  # pass dublicates and don't save them
  #if input in input_set or target in output_set:
  #  return None
  tokenized_inputs = tokenize_function(instructionModel, input)
  tokenized_outputs = tokenize_function(instructionModel, target)

  # filter out too long inout or outputs
  eos_id = instructionModel.sharedModel.config.eos_token_id
  if tokenized_inputs["input_ids"][-1] == eos_id or tokenized_outputs["input_ids"][-1] == eos_id:
    return None

  #tokenized_inputs["decoder_input_ids"] = tokenized_outputs.input_ids
  tokenized_inputs["decoder_attention_mask"] = tokenized_outputs.attention_mask
  tokenized_inputs["labels"] = tokenized_outputs.input_ids.copy()

  # because BERT automatically shifts the labels, the labels correspond exactly to `decoder_input_ids`.
  # For the purpose of training instructionBERT, we compute the loss only on the generated word tokens
  # and make sure that the PAD token is ignored by the loss function
  pad_id = instructionModel.sharedModel.config.pad_token_id
  tokenized_inputs["labels"] = [-100 if token == pad_id else token for token in tokenized_inputs["labels"]]

  unk_id = instructionModel.tokenizer.unk_token_id
  # no encoding or generation of unk tokens
  #if unk_id in tokenized_inputs["labels"] or unk_id in tokenized_inputs["input_ids"]:
  #  return None

  # no generation of unk tokens, but unk is encoded
  if unk_id in tokenized_inputs["labels"]:
    return None

  # no unk encoding
  if not encode_unk and unk_id in tokenized_inputs["input_ids"]:
    return None

  return tokenized_inputs


# 2 functions for the platypus dataset
def prefilter_platypus_for_no_translation(row):
  # run prefilters on the platypus dataset
  # we could try to concatenate additional input
  # for now just delete it
  if len(row["input"]) > 0:
    return False
  if "translate" in row["instruction"].lower() or "translation" in row["instruction"].lower():
  # we ignore translation tasks since we only have the english vocabulary
    #print(instruction["task_name"])
    return False
  else:
    return True

def get_platypus_dataset(instructionModel, filtered_dataset_text, save_tokenized=True):
  """
  download the dataset from huggingface and iterate over it.
  Save the text to the plain text list.
  Return the tokenized dataset.
  """
  platypus_dataset = datasets.load_dataset("garage-bAInd/Open-Platypus")
  filtered_platypus_dataset = platypus_dataset.filter(lambda example: prefilter_platypus_for_no_translation(example))

  list_platypus = []
  for number, line in enumerate(filtered_platypus_dataset["train"]):
    input = line["instruction"]
    target = line["output"]
    tokenized_dict = get_tokenized_dict(instructionModel, input, target)
    
    if tokenized_dict is not None:
      filtered_dataset_text.append([input, target])
      if save_tokenized:
        list_platypus.append(tokenized_dict)

  return list_platypus


# 2 functions for the Lima dataset
def prefilter_lima_for_no_translation(instruction):
  if "Translate" in instruction[0] or "translate into" in instruction[0]:
  # we ignore translation tasks since we only have the english vocabulary
    #print(instruction[0])
    return False
  else:
    return True

def get_lima_dataset(instructionModel, filtered_dataset_text, save_tokenized=True):
  raw_lima_dataset = datasets.load_dataset("GAIR/lima")
  filtered_lima_dataset = raw_lima_dataset.filter(lambda example: prefilter_lima_for_no_translation(example["conversations"]))

  # preprocess Lima
  list_lima = []
  for number, line in enumerate(filtered_lima_dataset["train"]):
    input = line["conversations"][0]
    target = line["conversations"][1]
    tokenized_dict = get_tokenized_dict(instructionModel, input, target)

    if tokenized_dict is not None:
      filtered_dataset_text.append([input, target])
      if save_tokenized:
        list_lima.append(tokenized_dict)
  return list_lima


# 2 functions for helpSteer dataset
def prefilter_helper_for_no_translation(row):
  # only keep longer output for better writing and reduce dublicates
  #if len(row["output"]) < 150:
  #  return False
  if "translate" in row["instruction"].lower() or "translation" in row["instruction"].lower():
  # we ignore translation tasks since we only have the english vocabulary
    #print(instruction["task_name"])
    return False
  else:
    return True

def get_helper_dataset(instructionModel, filtered_dataset_text, save_tokenized=True):
  prefiltered_helper_dataset = datasets.load_dataset("Weyaxi/HelpSteer-filtered")
  filtered_helper_dataset = prefiltered_helper_dataset.filter(lambda example: prefilter_helper_for_no_translation(example))
  list_helper = []

  for number, line in enumerate(filtered_helper_dataset["train"]):
    input = line["instruction"]
    target = line["output"]
    tokenized_dict = get_tokenized_dict(instructionModel, input, target)
    
    if tokenized_dict is not None:
      filtered_dataset_text.append([input, target])
      if save_tokenized:
        list_helper.append(tokenized_dict)

  return list_helper


# 2 functions for the FLAN dataset from Muennighoff
def prefilter_flan_for_no_translation(row):
  # only keep longer output for better writing and reduce dublicates
  #if len(row["output"]) < 150:
  #  return False
  if "translate" in row["inputs"].lower() or "translation" in row["inputs"].lower():
  # we ignore translation tasks since we only have the english vocabulary
    #print(instruction["task_name"])
    return False
  else:
    return True

def get_flan_dataset(instructionModel, filtered_dataset_text, flan_part = "validation", save_tokenized=True):
  flan_dataset = datasets.load_dataset("Muennighoff/flan", split=flan_part)
  filtered_flan_dataset = flan_dataset.filter(lambda example: prefilter_flan_for_no_translation(example))

  list_flan = []
  for number, line in enumerate(filtered_flan_dataset):
    input = line["inputs"]
    target = line["targets"]
    tokenized_dict = get_tokenized_dict(instructionModel, input, target)
    
    if tokenized_dict is not None:
      filtered_dataset_text.append([input, target])
      if save_tokenized:
        list_flan.append(tokenized_dict)
  return list_flan


# 2 functions for the orca dataset from open orca
def prefilter_orca_for_no_translation(row):
  # only keep longer output for better writing and reduce dublicates
  #if len(row["output"]) < 150:
  #  return False
  if "translate" in row["conversations"][1]["value"].lower() or "translation" in row["conversations"][1]["value"].lower():
  # we ignore translation tasks since we only have the english vocabulary
    return False
  else:
    return True

def get_orca_dataset(instructionModel, filtered_dataset_text, save_tokenized=True):
  orca_dataset = datasets.load_dataset("Open-Orca/SlimOrca-Dedup")
  filtered_orca_dataset = orca_dataset.filter(lambda example: prefilter_orca_for_no_translation(example))

  list_orca = []
  for number, line in enumerate(filtered_orca_dataset["train"]):
    input = line["conversations"][1]["value"]
    target = line["conversations"][2]["value"]
    tokenized_dict = get_tokenized_dict(instructionModel, input, target)
    
    if tokenized_dict is not None:
      filtered_dataset_text.append([input, target])
      if save_tokenized:
        list_orca.append(tokenized_dict)
  return list_orca


# 2 functions for the ultrachat dataset
def prefilter_ultrachat_for_no_translation(row):
  # only keep longer output for better writing and reduce dublicates
  #if len(row["output"]) < 150:
  #  return False
  instruction = row["prompt"].lower()
  if "translate" in instruction or "translation" in instruction:
  # we ignore translation tasks since we only have the english vocabulary
    return False
  else:
    return True

def get_ultrachat_dataset(instructionModel, filtered_dataset_text, save_tokenized=True):
  ultrachat = datasets.load_dataset("HuggingFaceH4/ultrachat_200k", split="train_sft")
  print(len(ultrachat))
  filtered_ultrachat_dataset = ultrachat.filter(lambda example: prefilter_ultrachat_for_no_translation(example))
  print(len(filtered_ultrachat_dataset))
  list_ultrachat = []
  for number, line in enumerate(filtered_ultrachat_dataset):
    combined_input = ""
    target = line["messages"][-1]["content"]
    assert line["messages"][-1]["role"] == "assistant"

    for message in line["messages"][:-1]:
      if message["role"] == "assistant":
        combined_input += "Answer:\n"
      elif message["role"] == "user":
        combined_input += "Question:\n"
      else:
        # doesn't happen
        print("unknown role in ultrachat:")
        print(message["role"])
      combined_input += message["content"] + "\n"
    combined_input += "Response:"
    tokenized_dict = get_tokenized_dict(instructionModel, combined_input, target)

    if tokenized_dict is not None:
      filtered_dataset_text.append([combined_input, target])
      if save_tokenized:
        list_ultrachat.append(tokenized_dict)
  return list_ultrachat


# 2 functions for the gpt4all dataset
def prefilter_gpt4all_for_no_translation(row):
  # only keep longer output for better writing and reduce dublicates
  #if len(row["output"]) < 150:
  #  return False
  inctruction = row["prompt"].lower()
  if "translate" in inctruction or "translation" in inctruction:
  # we ignore translation tasks since we only have the english vocabulary
    return False
  else:
    return True

def get_gpt4all_dataset(instructionModel, filtered_dataset_text, save_tokenized=True):
  groovy = datasets.load_dataset("nomic-ai/gpt4all-j-prompt-generations", revision='v1.3-groovy')
  filtered_gpt4all_dataset = groovy.filter(lambda example: prefilter_gpt4all_for_no_translation(example))

  list_groovy = []
  for number, line in enumerate(filtered_gpt4all_dataset["train"]):
    input = line["prompt"]
    target = line["response"]
    tokenized_dict = get_tokenized_dict(instructionModel, input, target)

    if tokenized_dict is not None:
      filtered_dataset_text.append([input, target])
      if save_tokenized:
        list_groovy.append(tokenized_dict)
  return list_groovy

# script to load the roleBench dataset
role_general_url = "https://huggingface.co/datasets/ZenMoore/RoleBench/raw/main/rolebench-eng/role-generalization/general/test.jsonl"
role_instruction_url = "https://huggingface.co/datasets/ZenMoore/RoleBench/raw/main/rolebench-eng/instruction-generalization/role_specific/train.jsonl"
generalization_instruction_url = "https://huggingface.co/datasets/ZenMoore/RoleBench/resolve/main/rolebench-eng/instruction-generalization/general/train.jsonl"

def load_dict_from_url(json_url):
  role_data = requests.get(json_url, verify=False)
  json_responses = role_data.text.split('\n')
  list_of_role_dicts = []
  for item in json_responses:
    if item != '':
      list_of_role_dicts.append(json.loads(item))

  return list_of_role_dicts

# one function for the roleBench dataset
def get_roleBench_dataset(instructionModel, filtered_dataset_text, save_tokenized=True):
  rolebench_data_list = []
  rolebench_data_list.extend(load_dict_from_url(role_general_url))
  rolebench_data_list.extend(load_dict_from_url(role_instruction_url))
  rolebench_data_list.extend(load_dict_from_url(generalization_instruction_url))
  print(len(rolebench_data_list))

  list_roleBench = []
  for number, line in enumerate(rolebench_data_list):
    if "translate" in line["question"].lower() or "translation" in line["question"].lower():
      pass
    else:
      input = "Roleplay as " + line["role"] + " and answer the following question:\n"
      input += line["question"]
      # choose the longer line of the first two lines
      if len(line["generated"])>1 and len(line["generated"][1]) > len(line["generated"][0]):
        target = line["generated"][1]
      else:
        target = line["generated"][0]

      tokenized_dict = get_tokenized_dict(instructionModel, input, target)

      if tokenized_dict is not None:
        filtered_dataset_text.append([input, target])
        if save_tokenized:
          list_roleBench.append(tokenized_dict)
  return list_roleBench


# 2 functions for the evol_instruct dataset
def prefilter_evol_instruct_for_no_translation(row):
  # only keep longer output for better writing and reduce dublicates
  #if len(row["output"]) < 150:
  #  return False
  instruction = row["conversations"][0]["value"].lower()
  if "translate" in instruction or "translation" in instruction:
  # we ignore translation tasks since we only have the english vocabulary
    return False
  else:
    return True

def get_evol_instruct_dataset(instructionModel, filtered_dataset_text, save_tokenized=True):
  evol_instruct = datasets.load_dataset("WizardLM/WizardLM_evol_instruct_V2_196k")
  filtered_evol_instruct_dataset = evol_instruct.filter(lambda example: prefilter_evol_instruct_for_no_translation(example))
  print(len(filtered_evol_instruct_dataset))

  list_evol_instruct = []
  for number, line in enumerate(filtered_evol_instruct_dataset["train"]):
    input = line["conversations"][0]["value"]
    target = line["conversations"][1]["value"]
    tokenized_dict = get_tokenized_dict(instructionModel, input, target)

    if tokenized_dict is not None:
      filtered_dataset_text.append([input, target])
      if save_tokenized:
        list_evol_instruct.append(tokenized_dict)
  return list_evol_instruct


# 2 functions for the alpaca_gpt4 dataset
def prefilter_alpaca_gpt4_for_no_translation(row):
  # run prefilters on the alpaca_gpt4 dataset
  # we could try to concatenate additional input
  # for now just delete it
  if len(row["input"]) > 0:
    return False

  instruction = row["instruction"].lower()
  if "translate" in instruction or "translation" in instruction:
  # we ignore translation tasks since we only have the english vocabulary
    return False
  else:
    return True

def get_alpaca_gpt4_dataset(instructionModel, filtered_dataset_text, save_tokenized=True):
  """
  download the dataset from huggingface and iterate over it.
  Save the text to the plain text list.
  Return the tokenized dataset.
  """
  alpaca_gpt4_dataset = datasets.load_dataset("c-s-ale/alpaca-gpt4-data")
  filtered_alpaca_gpt4_dataset = alpaca_gpt4_dataset.filter(lambda example: prefilter_alpaca_gpt4_for_no_translation(example))

  list_alpaca_gpt4 = []
  for number, line in enumerate(filtered_alpaca_gpt4_dataset["train"]):
    input = line["instruction"]
    target = line["output"]
    tokenized_dict = get_tokenized_dict(instructionModel, input, target)

    if tokenized_dict is not None:
      filtered_dataset_text.append([input, target])
      if save_tokenized:
        list_alpaca_gpt4.append(tokenized_dict)

  return list_alpaca_gpt4


# 2 functions for the agent instruct dataset
def prefilter_agent_instruct_for_no_translation(row):
  # only keep longer output for better writing and reduce dublicates
  #if len(row["output"]) < 150:
  #  return False
  instruction = row["conversations"][0]["value"].lower()
  if "translate" in instruction or "translation" in instruction:
  # we ignore translation tasks since we only have the english vocabulary
  # though there should be no translation agent instruction
    print("agent translation:")
    print(instruction)
    return False
  else:
    return True

agent_instruct_topics = ["os", "db", "alfworld", "webshop", "kg", "mind2web"]
topic_names = {
    "os" : "operating system",
    "db" : "database",
    "alfworld" : "aligning text and embodied environments for interactive learning",
    "webshop" : "webshop",
    "kg" : "knowledge graph",
    "mind2web" : "mind2web"
    }

def get_agent_instruct_dataset(instructionModel, filtered_dataset_text, save_tokenized=True):
  agent_instruct = datasets.load_dataset("THUDM/AgentInstruct")
  filtered_agent_instruct_dataset = agent_instruct.filter(lambda example: prefilter_agent_instruct_for_no_translation(example))

  list_agent_instruct = []

  for topic in agent_instruct_topics:
    for number, line in enumerate(filtered_agent_instruct_dataset[topic]):
      combined_input = "agent instruction for the topic of " + topic_names[topic]
      number_messages = len(line["conversations"])
      for message_number in range(0,number_messages,2):
        combined_input += "\ninstruction:\n" + line["conversations"][message_number]["value"]
        target = line["conversations"][message_number+1]["value"]
        tokenized_dict = get_tokenized_dict(instructionModel, combined_input, target)
        # save partial messages
        if tokenized_dict is not None:
          # filter the first line with 3 erronous messages of os
          # it occurs later on anyways ^^
          if not topic == "os" or not number == 0:
            filtered_dataset_text.append([combined_input, target])
            if save_tokenized:
              list_agent_instruct.append(tokenized_dict)
          # add the target to the input for further dialog rounds
        combined_input += "\nresponse:\n" + target
  return list_agent_instruct
