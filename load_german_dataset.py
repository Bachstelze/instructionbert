from datasets import load_dataset
import csv
import instructionBertClass
import dataset_methods
from sys import argv

script, model_name, max_length = argv

def get_filtered_bactrian_dataset(instructionModel, filtered_dataset_text, save_tokenized=True):
  bactrian = load_dataset("MBZUAI/Bactrian-X", "de")
  print(bactrian)
  list_german = []

  for number, line in enumerate(bactrian["train"]):
    if line["input"] == "":
      input = line["instruction"]
      target = line["output"]
      tokenized_dict = dataset_methods.get_tokenized_dict(instructionModel, input, target)

      if tokenized_dict is not None:
        filtered_dataset_text.append([input, target])
        if save_tokenized:
          list_german.append(tokenized_dict)

  return list_german

def get_filtered_german_aya_dataset(instructionModel, filtered_dataset_text, save_tokenized=True):
  aya = load_dataset("CohereForAI/aya_dataset")
  print(aya)
  list_aya = []

  for number, line in enumerate(aya["train"]):
    if line["language_code"] == "deu":
      input = line["inputs"]
      target = line["targets"]
      tokenized_dict = dataset_methods.get_tokenized_dict(instructionModel, input, target)

      if tokenized_dict is not None:
        filtered_dataset_text.append([input, target])
        if save_tokenized:
          list_aya.append(tokenized_dict)

  return list_aya

#dataset_names = ["Wiki-split-inst (T)", "Xlel_wd-inst", "Xlel_wd-inst (T)", "XWikis-inst", "HotpotQA (T)", "NQ-Open (T)"]
error_data = "Flan-Coqa (T)"
def get_filtered_aya_collection_dataset(instructionModel, filtered_dataset_text, save_tokenized=True):
  collection = load_dataset("CohereForAI/aya_collection_language_split", "german")
  print(collection)
  list_german = []

  for number, line in enumerate(collection["train"]):
    if line["dataset_name"] is not error_data:
      input = line["inputs"]
      target = line["targets"]
      tokenized_dict = dataset_methods.get_tokenized_dict(instructionModel, input, target)

      if tokenized_dict is not None:
        filtered_dataset_text.append([input, target])
        if save_tokenized:
          list_german.append(tokenized_dict)

  return list_german

def save_german_dataset(model_name, max_length):
    germanInstructionBERT = instructionBertClass.instructionBERT(model_name, max_length = max_length)

    plain_filtered_dataset_text = []
    get_filtered_bactrian_dataset(germanInstructionBERT, plain_filtered_dataset_text, save_tokenized=False)
    get_filtered_german_aya_dataset(germanInstructionBERT, plain_filtered_dataset_text, save_tokenized=False)#235
    get_filtered_aya_collection_dataset(germanInstructionBERT, plain_filtered_dataset_text, save_tokenized=False)#4305990
    print(len(plain_filtered_dataset_text))
    print(plain_filtered_dataset_text[-1])

    # save plain text as csv
    print("size of the final accumulated dataset:")
    print(len(plain_filtered_dataset_text))
    model_name_without_path = model_name.split("/")[-1]
    file_name = model_name_without_path + "_long_output_dataset.csv"
    with open(file_name,'w') as out:
        csv_out=csv.writer(out)
        #csv_out.writerow(['Instruction','Output'])
        for row in plain_filtered_dataset_text:
            csv_out.writerow(row)

    return plain_filtered_dataset_text

data_list = save_german_dataset(model_name, max_length)