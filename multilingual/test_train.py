from sys import argv
import csv
import sys
sys.path.append('../instructionbert')
import instructionbert.dataset_methods
import instructionbert.instructionBertClass
from transformers import Seq2SeqTrainer
from transformers import Seq2SeqTrainingArguments
from datasets import IterableDataset, Dataset
import gc
import torch

script, model_name, maximal_length, bool_fp16, optimizer, dataloader_workers, batch_size, warmup, lr, accumulation, epochs = argv

model_name_without_path = model_name.split("/")[-1]
file_name = model_name_without_path + "_long_output_dataset.csv"

with open(file_name, newline='') as file:
    reader = csv.reader(file)
    loaded_data_list = list(map(list, reader))

print("load the saved and prefiltered dataset from the csv")
print(len(loaded_data_list))
print(loaded_data_list[0])

multilingualInstructionBERT = instructionBertClass.instructionBERT(model_name, max_length = int(maximal_length))
pad_id = multilingualInstructionBERT.sharedModel.config.pad_token_id
tokenize_function = multilingualInstructionBERT.tokenize_function
validation_dataset= IterableDataset.from_generator(dataset_methods.stream_tokenized_dataset, gen_kwargs={"dataset":loaded_data_list[:100], "tokenize":tokenize_function, "pad_id":pad_id})
train_dataset= IterableDataset.from_generator(dataset_methods.stream_tokenized_dataset, gen_kwargs={"dataset":loaded_data_list[100:], "tokenize":tokenize_function, "pad_id":pad_id})

accumulated_batch_size = int(batch_size) * int(accumulation)
print("accumulated batch size: " + str(accumulated_batch_size))
print("data size without validation set: " + str(len(loaded_data_list) - 100))
max_train_steps = int((len(loaded_data_list) - 100)/accumulated_batch_size)
print("max train steps: " + str(max_train_steps))

#monolingualInstructionBERT = instructionBertClass.instructionBERT(model_name)
#validation_dataset, train_dataset = dataset_methods.get_tokenized_datasets(monolingualInstructionBERT, loaded_data_list)

training_args = Seq2SeqTrainingArguments(
    max_steps = max_train_steps,
    evaluation_strategy="steps",
    optim=optimizer,
    fp16=bool(bool_fp16),
    gradient_accumulation_steps=int(accumulation),
    learning_rate = float(lr), # default is 0.001 Leveraging Pre-trained Checkpoints has 0.05
    warmup_steps=int(warmup), # Leveraging Pre-trained Checkpoints has 40k
    per_device_train_batch_size=int(batch_size), # Leveraging Pre-trained Checkpoints has 128 or 256
    per_device_eval_batch_size=int(batch_size),
    output_dir="./",
    logging_steps=100,
    eval_steps=100,
    save_total_limit=1,
    num_train_epochs=int(epochs),
    dataloader_num_workers=int(dataloader_workers)
)

class MyTrainer(Seq2SeqTrainer):
    def log(self, logs):
        logs["learning_rate"] = self._get_learning_rate()
        super().log(logs)

# instantiate trainer
trainer = MyTrainer(
    model=multilingualInstructionBERT.sharedModel,
    args=training_args,
    train_dataset=train_dataset,
    eval_dataset=validation_dataset,
)
trainer.train()

#trainer.save_model()
# save seperate models
save_name = "./multilingualInstruction-" + model_name_without_path + "-lr" + str(lr)
multilingualInstructionBERT.sharedModel.save_pretrained(save_name)
multilingualInstructionBERT.tokenizer.save_pretrained(save_name)

test_input = ["Write a tweet for a new transformer model with hashtags #instruction #nlp #generation #encoder #decoder"
,"i'm 10x cooler than all of you! What is the sentiment of this tweet?"
,"Please answer the following question. What is the boiling point of Nitrogen?"
,"Please answer to the following question. Who is going to be the next Ballon d'or?"
,"Answer the following yes/no question. Can you write a whole Haiku in a single tweet?"
,"Premise:  At my age you will probably have learnt one lesson. Hypothesis:  It's not certain how many lessons you'll learn by your thirties. Does the premise entail the hypothesis?"
,"soil is a renewable resource for growing plants A plant that needs to expand will be able to have an endless resource in"
,"Q: Write a positive movie review. A: though overall an overwhelmingly positive portrayal Q: Write a negative movie review. A: nothing good can happen Q: Write a negative movie review. A:"
,"Write a short summary for this text: officials of the cabinet-level fair trade commission -lrb- ftc -rrb- said friday that they have formed an ad hoc group to investigate whether there is any manipulation of commodity prices by traders in local market . Summary:"
,"Solve 0 = 56*x + 16*x - 720 for x."
,"Hugo Käch died on December 31 , 2003 in Schaffhausen near Flurlingen , Germany . Hugo Käch died on 31 December 2003 in Flurlingen near Schaffhausen . Select your answer from the options. Do these sentences mean the same thing? Possible answers: - no; - yes;"
,"i'm 10x cooler than all of you! Select your answer from the options. What is the sentiment of this tweet? Available options: [i] negative [ii] positive...I think the answer is"
,"Q: What relation was British monarch Queen Victoria to Prince Albert before their marriage? A: first cousin Q: Originating in Eastern Europe, which pressed meat is seasoned with herbs and spices, smoked, and steamed after smoking? A: pastrami Q: What is the westernmost region of France? (not counting overseas territories) A:"
,"Shades of pink and purple Roses to charm and delight the special person in your life Add punctuation."]

def test_run(length_penalty):
  multilingualInstructionBERT.sharedModel.config.length_penalty = length_penalty

  file_name = str(length_penalty) + "_penalty_output.csv"
  with open(file_name,'w') as out:
    csv_out=csv.writer(out)
    csv_out.writerow(['Instruction','Output'])
    for input in test_input:
      print("\n")
      print(input)
      input_ids = multilingualInstructionBERT.tokenizer(input, return_tensors="pt").input_ids
      #print(input_ids)
      output_ids = multilingualInstructionBERT.sharedModel.generate(input_ids.cuda(), do_sample=True, max_new_tokens=maximal_length)
      #print(output_ids)
      output = multilingualInstructionBERT.tokenizer.decode(output_ids[0])
      print(output)
      csv_out.writerow([input, output])

test_run(1)
test_run(1.1)
test_run(1.25)
test_run(1.5)
test_run(2)

# clear the gpu cache
torch.cuda.empty_cache()
gc.collect()
