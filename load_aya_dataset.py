import datasets
import csv
import instructionBertClass
import dataset_methods
from sys import argv

script, model_name, max_length = argv

# edit those languages for xlm-r
mbert_unknown_lang_code = ["sin","amh","som","hau","ibo","pbt"]

def get_filtered_aya_dataset(instructionModel, filtered_dataset_text, save_tokenized=True):
  aya = datasets.load_dataset("CohereForAI/aya_dataset")
  print(aya)
  list_aya = []

  for number, line in enumerate(aya["train"]):
    input = line["inputs"]
    target = line["targets"]
    tokenized_dict = dataset_methods.get_tokenized_dict(instructionModel, input, target)

    if line["language_code"] not in mbert_unknown_lang_code and tokenized_dict is not None:
      filtered_dataset_text.append([input, target])
      if save_tokenized:
        list_aya.append(tokenized_dict)
    
  return list_aya

def save_multilingual_dataset(model_name, max_length):
    multilingualInstructionBERT = instructionBertClass.instructionBERT(model_name, max_length = max_length)

    plain_filtered_dataset_text = []
    get_filtered_aya_dataset(multilingualInstructionBERT, plain_filtered_dataset_text, save_tokenized=False)
    print(len(plain_filtered_dataset_text))
    print(plain_filtered_dataset_text[-1])

    # save plain text as csv
    print("size of the final accumulated dataset:")
    print(len(plain_filtered_dataset_text))
    model_name_without_path = model_name.split("/")[-1]
    file_name = model_name_without_path + "_long_output_dataset.csv"
    with open(file_name,'w') as out:
        csv_out=csv.writer(out)
        #csv_out.writerow(['Instruction','Output'])
        for row in plain_filtered_dataset_text:
            csv_out.writerow(row)

    return plain_filtered_dataset_text

data_list = save_multilingual_dataset(model_name, int(max_length))
