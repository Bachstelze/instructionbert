import datasets
import csv
import instructionBertClass as instructionBertClass
import dataset_methods as dataset_methods
from sys import argv

script, model_name, max_length, sorting = argv

xmr_unknown_lang_code = ["yor", "ibo", "zul", "wol", "nya", "sna"]
# size of the final accumulated dataset: 170911

def get_filtered_aya_dataset(instructionModel, filtered_dataset_text, save_tokenized=True):
  aya = datasets.load_dataset("CohereForAI/aya_dataset")
  print(aya)
  list_aya = []

  for number, line in enumerate(aya["train"]):
    input = line["inputs"]
    target = line["targets"]
    tokenized_dict = dataset_methods.get_tokenized_dict(instructionModel, input, target, encode_unk=False)

    if line["language_code"] not in xmr_unknown_lang_code:
      if tokenized_dict is not None:
        filtered_dataset_text.append([input, target])
        if save_tokenized:
          list_aya.append(tokenized_dict)
      else:
        pass
        #print(line)
        #print(line["language"])
        #print(line["language_code"])

  return list_aya

#size of the final accumulated dataset: 372565
high_bactrian = ["de", "ar", "id", "zh", "es", "fr", "ja", "pt", "ru", "ko", "mk", "hr", "hi"]
def get_filtered_bactrian_dataset(instructionModel, filtered_dataset_text, save_tokenized=True):
  all_lang_list = []
  for lang in high_bactrian:
    bactrian = datasets.load_dataset("MBZUAI/Bactrian-X", lang)
    #print(bactrian)
    print("bactrian language code:")
    print(lang)

    for number, line in enumerate(bactrian["train"]):
      if line["input"] == "":
        input = line["instruction"]
        target = line["output"]
        tokenized_dict = dataset_methods.get_tokenized_dict(instructionModel, input, target, encode_unk=False)

        if tokenized_dict is not None:
          filtered_dataset_text.append([input, target])
          if save_tokenized:
            all_lang_list.append(tokenized_dict)

  return all_lang_list

#size of the final accumulated dataset: 402597
def get_filtered_towerblock_dataset(instructionModel, filtered_dataset_text, save_tokenized=True):
  towerblock = datasets.load_dataset("Unbabel/TowerBlocks-v0.2")
  print(towerblock)
  list_towerblock = []

  for number, line in enumerate(towerblock["train"]):
    conversations = line["conversations"]
    if len(conversations) == 2:
      input = conversations[0]["value"]
      target = conversations[1]["value"]
      if input and target:
        tokenized_dict = dataset_methods.get_tokenized_dict(instructionModel, input, target, encode_unk=False)
        if tokenized_dict is not None:
          filtered_dataset_text.append([input, target])
          if save_tokenized:
            list_towerblock.append(tokenized_dict)
      else:
        print(number)
        print(line)

  return list_towerblock

def save_multilingual_dataset(model_name, max_length):
    multilingualInstructionBERT = instructionBertClass.instructionBERT(model_name, max_length = max_length)

    plain_filtered_dataset_text = []
    get_filtered_aya_dataset(multilingualInstructionBERT, plain_filtered_dataset_text, save_tokenized=False)
    get_filtered_bactrian_dataset(multilingualInstructionBERT, plain_filtered_dataset_text, save_tokenized=False)
    get_filtered_towerblock_dataset(multilingualInstructionBERT, plain_filtered_dataset_text, save_tokenized=False)
    print(len(plain_filtered_dataset_text))
    print(plain_filtered_dataset_text[-1])

    if sorting == "True":
      #sort dataset by length
      plain_filtered_dataset_text.sort(key=lambda s: len(s[0])+len(s[1]))

    # save plain text as csv
    print("size of the final accumulated dataset:")
    print(len(plain_filtered_dataset_text))
    model_name_without_path = model_name.split("/")[-1]
    file_name = model_name_without_path + "_long_output_dataset.csv"
    with open(file_name,'w') as out:
        csv_out=csv.writer(out)
        #csv_out.writerow(['Instruction','Output'])
        for row in plain_filtered_dataset_text:
            csv_out.writerow(row)

    return plain_filtered_dataset_text

data_list = save_multilingual_dataset(model_name, max_length)